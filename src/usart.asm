;   USART - Send and recieve data using the USART module of the PIC
;   Copyright (C) 2009  Steven Rodriguez
;
;   This program is part of USART
;
;   This program is free software: you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation, either version 3 of the License, or
;   (at your option) any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program.  If not, see <http://www.gnu.org/licenses/>.
;
;   If this program has problems please contact me at:
;
;   stevencrc@digitecnology.zapto.org

;==============================================
;USART (2009 - Steven Rodriguez)
;==============================================

;General description
;=============================
; The display leds are connected to:
; - RB0 - 6 - bit 0 of the counter
; - RB1 - 7 - RX of the USART
; - RB2 - 8 - TX of the USART
; - RB3 - 9 - bit 3 of the counter
; - RA4 - 3 - Switch to change the counter value
; - RB5 - 11 - bit 5 of the counter
; - RB6 - 12 - bit 6 of the counter
; - RB7 - 13 - bit 7 of the counter
; - RA1 - 18 - bit 1 of the counter
; - RA2 - 1 - bit 2 of the counter
; - RA3 - 2 - Switch to change the counter value
; - RA4 - 3 - Switch to send the counter value through the USART interface
; - RA6 - 15 - LED to indicate transfer status
; - RA7 - 17 - LED to indicate recieve status
;=============================

;Variables
;==============================
COUNTER EQU 0x27
TEMP EQU 0x28
BTNPRESSED EQU 0x29
LASTBYTE EQU 0x2A

;Assembler directives
;==============================
	list p=16f628a
        include p16f628a.inc
        __config b'11111100111000'
        org 0x00 ;Reset vector
        goto Start
	org 0x04 ;Interrupt vector
	goto Interrupt

;Functions
;==============================
	include digitecnology.inc
ChangeCounter
	call chgbnk0
	;Turn off transfer and recieve LED's
	bcf PORTA,6
	bcf PORTA,7
	incf COUNTER,1
	return
ShowCounter
	call chgbnk0
	movf COUNTER,0
	andlw b'11111001'
	movwf TEMP
	movf PORTB,0
	andlw b'00000110'
	iorwf TEMP,0
	movwf PORTB
	movf COUNTER,0
        andlw b'00000110'
        movwf TEMP
        movf PORTA,0
        andlw b'11111001'
        iorwf TEMP,0
        movwf PORTA
	return
SendCounter
	call chgbnk0
	movf COUNTER,0
	movwf TXREG
	call chgbnk1
	btfsc TXSTA,1
	goto $-1
	call chgbnk0
	bsf PORTA,6 ;Turn on transfer LED
	return
Wait
	;Wait 10 ms
	movlw .246
	movwf DELAY1
	movlw .14
	movwf DELAY2
	call delay2
	return
;Program
;==============================
Interrupt
	bcf INTCON,7 ;Disable global interrupts
	call chgbnk0
	btfss PIR1,5 ;Check if recieve
	goto $+0x12
	;Move recieve register to LASTBYTE
	movf RCREG,0
	movwf LASTBYTE
	;Show the last byte recieved
        movf LASTBYTE,0
        andlw b'11111001'
        movwf TEMP
        movf PORTB,0
        andlw b'00000110'
        iorwf TEMP,0
        movwf PORTB
        movf LASTBYTE,0
        andlw b'00000110'
        movwf TEMP
        movf PORTA,0
        andlw b'11111001'
        iorwf TEMP,0
        movwf PORTA
	bsf PORTA,7 ;Turn on recieve LED
	bsf INTCON,7 ;Enable global interrupts
	retfie
Start ;Main program
	goto Initialize
Initialize
	;Change to bank 0
	call chgbnk0
	;Clear PORTA and PORTB
	clrf PORTA
	clrf PORTB
	;Change to bank 1
	call chgbnk1
	;Set inputs/outputs
	bcf TRISB,0 ;Output
	bsf TRISB,1 ;Input
	bcf TRISB,2 ;Output
	bcf TRISB,3 ;Output
	bcf TRISB,4 ;Output
	bcf TRISB,5 ;Output
	bcf TRISB,6 ;Output
	bcf TRISB,7 ;Output
	bcf TRISA,1 ;Output
	bcf TRISA,2 ;Output
	bsf TRISA,3 ;Input
	bsf TRISA,4 ;Input
	bcf TRISA,6 ;Output
	bcf TRISA,7 ;Output
	;Deactivate analog comparators
	call chgbnk0
	bsf CMCON,2
	bsf CMCON,1
	bsf CMCON,0
	;Clear and show counter
	clrf COUNTER
	call ShowCounter
	;Set interrupts
	bsf INTCON,6 ;Enable peripheral interrupts
	call chgbnk1
	bsf PIE1,5 ;USART recieve interrupt enable
	;Initialize USART
	bcf TXSTA,6 ;Enable 8 bit transmission
	bcf TXSTA,4 ;Asynchronus mode
	bsf TXSTA,2 ;High baud rate
	bsf TXSTA,5 ;Enable transmission
	movlw .25 ;Set baud rate 9600
	movwf SPBRG
	call chgbnk0
	bsf RCSTA,4 ;Countinuous recieve
	bcf RCSTA,6 ;8 bit recieve
	bsf RCSTA,7 ;Enable serial port
	;Enable global interrupts
	call chgbnk0
	bsf INTCON,7
Cycle
	;Check input (counter change)
	btfsc PORTA,3
	goto $+0xA ;10
	call Wait
	btfsc PORTA,3
	goto $+7
	btfsc BTNPRESSED,0
	goto $+6
	call ChangeCounter
	call ShowCounter
	bsf BTNPRESSED,0
	goto $+2
	bcf BTNPRESSED,0
	;Check input (USART send)
        btfsc PORTA,4
        goto $+9
        call Wait
        btfsc PORTA,4
        goto $+6
        btfsc BTNPRESSED,1
        goto $+5
        call SendCounter
        bsf BTNPRESSED,1
        goto $+2
        bcf BTNPRESSED,1
	goto Cycle
	end

